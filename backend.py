import glfw
import OpenGL.GL as gl
import imgui
from imgui.integrations.glfw import GlfwRenderer
import sys

class InputState:
    def __init__(self):
        self.keys_down = set()
        self.keys_held = set()
        
        self.mouse_buttons_down = [False, False, False]
        self.mouse_buttons_held = [False, False, False]

        self.shift = False
        self.ctrl = False
        self.alt = False

    def key_down(self, key):
        return key in self.keys_down

    def key_held(self, key):
        return key in self.keys_held

    def mouse_down(self, button):
        return self.mouse_buttons_down[button]

    def mouse_held(self, button):
        return self.mouse_buttons_held[button]

    def on_key(self, key, action):
        if action == glfw.PRESS:
            self.keys_down.add(key)
            self.keys_held.add(key)
        if action == glfw.RELEASE:
            self.keys_held.remove(key)

    def on_mouse_button(self, button, action):
        if button >= 0 and button <= 2:
            if action == glfw.PRESS:
                self.mouse_buttons_down[button] = True
                self.mouse_buttons_held[button] = True
            elif action == glfw.RELEASE:
                self.mouse_buttons_held[button] = False

    def reset_transient_state(self):
        self.keys_down.clear()
        self.mouse_buttons_down[0] = False
        self.mouse_buttons_down[1] = False
        self.mouse_buttons_down[2] = False

    def process_inputs(self):
        self.shift = (glfw.KEY_LEFT_SHIFT in self.keys_held or glfw.KEY_RIGHT_SHIFT in self.keys_held)
        self.ctrl = (glfw.KEY_LEFT_CONTROL in self.keys_held or glfw.KEY_RIGHT_CONTROL in self.keys_held)
        self.alt = (glfw.KEY_LEFT_ALT in self.keys_held or glfw.KEY_RIGHT_ALT in self.keys_held)
        self.super = (glfw.KEY_LEFT_SUPER in self.keys_held or glfw.KEY_RIGHT_SUPER in self.keys_held)

class ExtendedGlfwRenderer(GlfwRenderer):
    def __init__(self, window):
        super(ExtendedGlfwRenderer, self).__init__(window, False)
        self.input_state = InputState()

        glfw.set_key_callback(self.window, self.keyboard_callback)
        glfw.set_mouse_button_callback(self.window, self.mouse_button_callback)
        glfw.set_cursor_pos_callback(self.window, self.mouse_callback)
        glfw.set_window_size_callback(self.window, self.resize_callback)
        glfw.set_char_callback(self.window, self.char_callback)
        glfw.set_scroll_callback(self.window, self.scroll_callback)

    def keyboard_callback(self, window, key, scancode, action, mods):
        self.input_state.on_key(key, action)
        super(ExtendedGlfwRenderer, self).keyboard_callback(window, key, scancode, action, mods)

    def mouse_button_callback(self, window, button, action, mods):
        self.input_state.on_mouse_button(button, action)

    def process_inputs(self):
        super(ExtendedGlfwRenderer, self).process_inputs()
        self.input_state.process_inputs()


def run(setup, draw_frame, cleanup, window_size=(1280, 720), window_name="window"):
    window = impl_glfw_init(window_size[0], window_size[1], window_name)
    impl = ExtendedGlfwRenderer(window)

    setup()

    while not glfw.window_should_close(window):
        impl.input_state.reset_transient_state()

        glfw.poll_events()
        impl.process_inputs()

        imgui.new_frame()

        keep_running = draw_frame(impl.input_state)
        if not keep_running:
            glfw.set_window_should_close(impl.window, True)

        gl.glClearColor(1., 1., 1., 1)
        gl.glClear(gl.GL_COLOR_BUFFER_BIT)

        imgui.render()
        impl.render(imgui.get_draw_data())
        glfw.swap_buffers(window)

    impl.shutdown()

    cleanup()

    glfw.terminate()

def impl_glfw_init(width, height, window_name):
    if not glfw.init():
        print("Could not initialize OpenGL context")
        exit(1)

    # OS X supports only forward-compatible core profiles from 3.2
    glfw.window_hint(glfw.CONTEXT_VERSION_MAJOR, 3)
    glfw.window_hint(glfw.CONTEXT_VERSION_MINOR, 3)
    glfw.window_hint(glfw.OPENGL_PROFILE, glfw.OPENGL_CORE_PROFILE)

    glfw.window_hint(glfw.OPENGL_FORWARD_COMPAT, gl.GL_TRUE)

    # Create a windowed mode window and its OpenGL context
    window = glfw.create_window(
        int(width), int(height), window_name, None, None
    )
    glfw.make_context_current(window)

    if not window:
        glfw.terminate()
        print("Could not initialize Window")
        exit(1)

    return window