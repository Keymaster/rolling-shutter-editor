This project is an editor that creates an image from a video which imitates the [rolling-shutter effect](https://en.wikipedia.org/wiki/Rolling_shutter) in a more general form.

**Principle**: A "sweeper" element, such as a line, moves across image over time. At each position of the line, the pixels of the current video frame under the line are copied to the output image. 

In contrast to the real rolling-shutter effect, which typically is a horizontal line moving up or down the frame, the sweepers implemented here are more general, i.e. the line can move over any region of the video, at any speed.  
Additionally, an ellipse sweeper is provided, which is an elliptic outline moving inwards or outwards.

## Setup
Note: This has only been tested in python 3.7, on MacOS High Sierra. I can't guarantee that it works on any other setup.

### Pre-requisites:

- Install [git-lfs (https://git-lfs.github.com/)](https://git-lfs.github.com/)
	- Required for cloning pyimgui.
- Install make (On MacOS, usually done via xcode)
	- Required to build pyimgui.

### Installation:

- Clone this repository.
- In the repository, run:
	- `git submodule init`
	- `git submodule update`
	- These will init and clone the pyimgui dependency.
- Create a virtual environment and activate it. (Technically optional, but strongly recommended)
	- Create it with: `python -m venv env` (For python 3)
	- Activate it with: `source env/bin/activate`
- Build pyimgui
	- Make sure you are in the pyimgui subfolder (which was created by the git submodule commands).
	- Run `make build`
	- This will build pyimgui, and install this local copy as a package in the virtual environment.
- Install the remaining requirements via pip.
	- From the repository root folder, run: `pip install -r requirements.txt`
- Done!

## Execution	
Run `python main.py <path to video file>` with the virtual environment activated. This will launch the editor. 

## Editor usage
The basic workflow is as follows:

- Select a sweeper from the window on the right.
	- Currently, a moving line and a scaling ellipse are available.
- Position the start and end of the sweeper as desired using the handles.
- Find the desired start and end frame of the sweep using the video timeline at the bottom, and set the start and end frames of the sweeper using the buttons in the sweeper window.
- Click "Do sweep" in the bottom right, which will create the sweeped image. The image will be saved as "img_out.png" in the folder from which you ran main.py
	- Note that this will overwrite whatever other file was previously generated.

The elements of the UI are explained below:
![Overview of editor UI](UI_overview.png)

Controls and keyboard shortcuts:

- Mouse wheel scrolling: Zoom the video.
- Middle mouse button + Drag: Pan the video.
- Space: Play/Pause video
- Left / Right arrow: Go to next / previous frame
- Cmd + Left arrow / Cmd + Right arrow: Jump to start / end of the video.