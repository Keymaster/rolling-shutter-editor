from utils import clamp

def t_at_frame(sweeper, frame):
    t = (frame - sweeper.start_frame) / sweeper.move_time
    return clamp(t, 0, 1)

def is_close(a, b, thresh):
    thresh = thresh ** 2

    return (a - b).length_sqr() <= thresh