import cv2
import attr
from video_info import VideoInfo

@attr.s
class VideoReader():
    reader = attr.ib()
    frame_idx = attr.ib(default=-1, init=False)
    frame_data = attr.ib(default=None, init=False)

    info = attr.ib(init=False)

    @info.default
    def _info_default(self):
        return VideoInfo.from_capture(self.reader)

    @classmethod
    def from_path(cls, path):
        reader = cv2.VideoCapture(path)
        if not reader.isOpened():
            print("Could not read " + path)
            return None

        reader.set(cv2.CAP_PROP_CONVERT_RGB, True)
        return cls(reader)

    def seek_to(self, frame_idx):
        if frame_idx < 0:
            frame_idx = 0
        if frame_idx >= self.info.frame_count:
            frame_idx = self.info.frame_count - 1

        if self.frame_idx != frame_idx - 1:
            self.reader.set(cv2.CAP_PROP_POS_FRAMES, frame_idx)
            self.frame_idx = frame_idx - 1

        self.step_frame()

    def step_frame(self):
        if self.frame_idx + 1 < self.info.frame_count:
            self.frame_idx += 1
            _, self.frame_data = self.reader.read()

            return True
        return False

    def release(self):
        self.reader.release()

class VideoPlayer():
    def __init__(self, file_path):
        self.reader = VideoReader.from_path(file_path)

        self.time_accum = 0
        self.paused = True

        self.reader.seek_to(0)

    def get_decimal_frame(self):
        return self.reader.frame_idx + self.time_accum / self.reader.info.frame_delta

    def seek_to(self, frame_idx):
        self.reader.seek_to(frame_idx)
        self.time_accum = 0

    def step_time(self, dt):
        if self.paused:
            return False

        self.time_accum += dt

        stepped = False
        while self.time_accum > self.reader.info.frame_delta:
            self.time_accum -= self.reader.info.frame_delta
            # self.time_accum = 0 # quick fix to prevent time accumulating indefinitely when 

            stepped = self.reader.step_frame()
            if not stepped:
                self.paused = True

        return stepped

    def next_frame(self):
        self.reader.step_frame()
        self.time_accum = 0

    def prev_frame(self):
        self.seek_to(self.reader.frame_idx - 1)

    def seek_to_start(self):
        self.seek_to(0)

    def seek_to_end(self):
        self.seek_to(self.reader.info.frame_count - 1)

    def cleanup(self):
        self.reader.release()