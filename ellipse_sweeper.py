import attr
import cv2
import imgui
import numpy as np
from vec2 import V2
from math import ceil
from sweeper_utils import t_at_frame, is_close

ellipse_draw_arc_length = 15

@attr.s
class Ellipse(object):
    center = attr.ib()
    radii = attr.ib()
    angle = attr.ib()

    def get_points(self, arc_length):
            # find the angle delta to use for approximating the ellipse to a polyline
            # the delta is found as that which gives the desired arc length as a arc length on a circle with the greater of the two ellipse radii
            # the greater radius is picked since it will give the smaller delta and thus the finer approximation
        max_radius = max(self.radii.x, self.radii.y)

        angle_delta = arc_length / max_radius

        steps = ceil((2 * np.pi) / angle_delta)

        if steps > 1024:
            steps = 1024
            angle_delta = 2 * np.pi / steps

        angles = np.arange(steps) * angle_delta
        points = np.vstack([np.cos(angles), np.sin(angles)])
        points[0,:] *= self.radii.x
        points[1,:] *= self.radii.y

        c = np.cos(self.angle)
        s = np.sin(self.angle)
        rot_mat = np.array([[c, -s], [s, c]])

        points = rot_mat @ points

        points += np.array([[self.center.x], [self.center.y]])

        return points.T

    def scaled(self, scale):
        return Ellipse(self.center, self.radii * scale, self.angle)

    def match_larger_axis_length(self, axis_length):
        larger_axis = max(self.radii.x, self.radii.y)
        scale = axis_length / larger_axis

        return self.scaled(scale)

@attr.s
class EllipseSweeper(object):
    start_ellipse = attr.ib()
    move_scale = attr.ib()

    start_frame = attr.ib()
    move_time = attr.ib()

    def ellipse_at_frame(self, frame):
        return self.ellipse_at_t(t_at_frame(self, frame))

    def ellipse_at_t(self, t):
        scale = 1 * (1 - t) + t * self.move_scale
        return self.start_ellipse.scaled(scale)

    def mask_iter(self, mask):
        larger_radius = max(self.start_ellipse.radii.x, self.start_ellipse.radii.y)
        most_distance_traversed = np.abs(larger_radius * self.move_scale - larger_radius)

        dist_per_time = most_distance_traversed / self.move_time

        if dist_per_time < 1:
            steps = ceil(most_distance_traversed) + 1

            prev_ellipse = self.start_ellipse
            for distance in range(steps):
                radius_move = min(distance, most_distance_traversed)
                t = radius_move / most_distance_traversed

                ellipse = self.ellipse_at_t(t)
                frame = int(self.start_frame + t * self.move_time)

                mask.fill(0)
                if self.move_scale < 1:
                    self.fill_ellipses(mask, prev_ellipse, ellipse)
                else:
                    self.fill_ellipses(mask, ellipse, prev_ellipse)

                prev_ellipse = ellipse

                yield frame
        else:
            prev_ellipse = self.start_ellipse
            for frame in range(self.start_frame, self.start_frame + self.move_time + 1):
                ellipse = self.ellipse_at_frame(frame)

                mask.fill(0)
                if self.move_scale < 1:
                    self.fill_ellipses(mask, prev_ellipse, ellipse)
                else:
                    self.fill_ellipses(mask, ellipse, prev_ellipse)
                    
                prev_ellipse = ellipse

                yield frame

    def fill_ellipses(self, mask, larger_ellipse, smaller_ellipse):
        points_larger = np.int32(larger_ellipse.get_points(ellipse_draw_arc_length))
        points_smaller = np.int32(smaller_ellipse.get_points(ellipse_draw_arc_length))

        cv2.fillConvexPoly(mask, points_larger, 255)
        cv2.fillConvexPoly(mask, points_smaller, 0)

    def draw_rest_mask(self, mask):
        mask.fill(0)
        end_ellipse = self.ellipse_at_t(1)

        if self.move_scale >= 1:
            diagonal = mask.shape[0] + mask.shape[1]
            far_ellipse = self.start_ellipse.match_larger_axis_length(diagonal)
            self.fill_ellipses(mask, far_ellipse, end_ellipse)
        else:
            points = np.int32(end_ellipse.get_points(ellipse_draw_arc_length))
            cv2.fillConvexPoly(mask, points, 255)

@attr.s
class EllipseSweeperControls(object):
    sweeper = attr.ib()
    name = attr.ib(default='Ellipse')

    dragging_center = attr.ib(default=False)
    dragging_angle  = attr.ib(default=False)
    dragging_start_scale = attr.ib(default=False)
    dragging_y_size = attr.ib(default=False)
    dragging_end_scale  = attr.ib(default=False)

    def draw_ellipse(self, draw_list, ellipse, color):
        points = ellipse.get_points(ellipse_draw_arc_length)
        
        for i in range(len(points)):
            a = points[i]
            b = points[(i+1)%len(points)]

            draw_list.add_line(V2.from_tuple(a), V2.from_tuple(b), color)

    def draw_and_interact(self, draw_list, frame_time, frame_size, input_state):
        cur_shape_color = (1,1,1,1)
        handle_shape_color = (0.6, 0.6, 1.0, 1)

        start_ellipse = self.sweeper.start_ellipse
        end_ellipse = self.sweeper.ellipse_at_t(1)

        mouse_image_pos = draw_list.invert_point(V2.from_tuple(imgui.get_mouse_pos()))
        handle_radius = draw_list.invert_size(5)
        handle_offset = draw_list.invert_size(10)

        center_handle_pos = start_ellipse.center

        x_size_dir = V2.from_angle(start_ellipse.angle)
        y_size_dir = x_size_dir.perpendicular()

        start_scale_handle_pos = center_handle_pos + x_size_dir * start_ellipse.radii.x
        y_size_handle_pos = center_handle_pos + y_size_dir * start_ellipse.radii.y

        angle_handle_pos = center_handle_pos + x_size_dir * (start_ellipse.radii.x + handle_offset)

        end_scale_handle_pos = center_handle_pos + x_size_dir * end_ellipse.radii.x

        if not input_state.mouse_held(0):
            self.dragging_center = False
            self.dragging_angle = False
            self.dragging_start_scale = False
            self.dragging_y_size = False
            self.dragging_end_scale = False

        if not self.dragging_any() and input_state.mouse_down(0):
            drag_thresh = handle_radius ** 2
            if is_close(center_handle_pos, mouse_image_pos, handle_radius):
                self.dragging_center = True
            if is_close(angle_handle_pos, mouse_image_pos, handle_radius):
                self.dragging_angle = True
            if is_close(start_scale_handle_pos, mouse_image_pos, handle_radius):
                self.dragging_start_scale = True
            if is_close(y_size_handle_pos, mouse_image_pos, handle_radius):
                self.dragging_y_size = True
            if is_close(end_scale_handle_pos, mouse_image_pos, handle_radius):
                self.dragging_end_scale = True

        if self.dragging_center:
            center_handle_pos = mouse_image_pos
            self.sweeper.start_ellipse.center = center_handle_pos

        if self.dragging_angle:
            angle_handle_pos = mouse_image_pos
            self.sweeper.start_ellipse.angle = (angle_handle_pos - center_handle_pos).angle()

        if self.dragging_start_scale:
            projected_distance = abs((mouse_image_pos - center_handle_pos).dot(x_size_dir))
            start_scale_handle_pos = center_handle_pos + x_size_dir * projected_distance

            scale_change = projected_distance / self.sweeper.start_ellipse.radii.x
            radii_ratio = self.sweeper.start_ellipse.radii.y / self.sweeper.start_ellipse.radii.x

            self.sweeper.start_ellipse.radii.x = projected_distance
            self.sweeper.start_ellipse.radii.y = projected_distance * radii_ratio
            self.sweeper.move_scale *= 1 / scale_change

        if self.dragging_y_size:
            projected_distance = abs((mouse_image_pos - center_handle_pos).dot(y_size_dir))
            y_size_handle_pos = center_handle_pos + y_size_dir * projected_distance
            self.sweeper.start_ellipse.radii.y = projected_distance

        if self.dragging_end_scale:
            projected_distance = abs((mouse_image_pos - center_handle_pos).dot(x_size_dir))
            end_scale_handle_pos = center_handle_pos + x_size_dir * projected_distance
            self.sweeper.move_scale = projected_distance / start_ellipse.radii.x

        start_ellipse = self.sweeper.start_ellipse
        end_ellipse = self.sweeper.ellipse_at_t(1)
        cur_ellipse = self.sweeper.ellipse_at_frame(frame_time)

        self.draw_ellipse(draw_list, start_ellipse, handle_shape_color)
        self.draw_ellipse(draw_list, end_ellipse, handle_shape_color)
        self.draw_ellipse(draw_list, cur_ellipse, cur_shape_color)

        handle_color = (1,1,1,1)
        draw_list.add_circle_filled(center_handle_pos, handle_radius, handle_color)
        draw_list.add_circle_filled(start_scale_handle_pos, handle_radius, handle_color)
        draw_list.add_circle_filled(y_size_handle_pos, handle_radius, handle_color)
        draw_list.add_circle_filled(angle_handle_pos, handle_radius, handle_color)
        draw_list.add_circle_filled(end_scale_handle_pos, handle_radius, handle_color)

    def dragging_any(self):
        return self.dragging_center or self.dragging_angle or self.dragging_start_scale or self.dragging_y_size or self.dragging_end_scale