import attr
from vec2 import V2
import imgui
import cv2
import numpy as np
from sweeper_utils import t_at_frame, is_close
from math import ceil

@attr.s
class Line(object):
    pos = attr.ib()
    normal_angle = attr.ib()

    def get_points(self, distance):
        along = self.normal().rotate90()
        return self.pos - along * distance, self.pos + along * distance

    def normal(self):
        return V2.from_angle(self.normal_angle)

    def offset_line(self, distance):
        pos = self.pos + self.normal() * distance

        return Line(pos, self.normal_angle)

@attr.s
class LineSweeper(object):
    start_line = attr.ib()
    move_dist = attr.ib()

    start_frame = attr.ib()
    move_time = attr.ib()

    def line_at_frame(self, frame):
        return self.line_at_t(t_at_frame(self, frame))

    def line_at_t(self, t):
        return self.start_line.offset_line(t * self.move_dist)

        # iterator over lines between start_line and the implicitly defined end line (start_line offset by move_dist)
        # lines are returned at every step of 1 along either space or time
        # i.e. if dist / time < 1, the line moves less than 1 px per frame. Then, each line returned is 1 px apart
        # if dist / time >= 1, each line returned is on the next frame
    def step_lines_iter(self):
        dist_per_time = self.move_dist / self.move_time

        if dist_per_time < 1:
            steps = ceil(self.move_dist) + 1

            for distance in range(steps):
                offset = min(distance, self.move_dist)

                line = self.start_line.offset_line(offset)

                t = offset / self.move_dist
                frame = int(self.start_frame + t * self.move_time)

                yield line, frame
        else:
            for frame in range(self.start_frame, self.start_frame + self.move_time + 1):
                line = self.line_at_frame(frame)

                yield line, frame

    def mask_iter(self, mask):
        dist_per_time = self.move_dist / self.move_time

        if dist_per_time < 1:
            steps = ceil(self.move_dist) + 1

            prev_line = self.start_line
            for distance in range(steps):
                offset = min(distance, self.move_dist)

                line = self.start_line.offset_line(offset)

                t = offset / self.move_dist
                frame = int(self.start_frame + t * self.move_time)

                mask.fill(0)
                self.fill_lines(mask, prev_line, line)
                prev_line = line

                yield frame
        else:
            prev_line = self.start_line
            for frame in range(self.start_frame, self.start_frame + self.move_time + 1):
                line = self.line_at_frame(frame)

                mask.fill(0)
                self.fill_lines(mask, prev_line, line)
                prev_line = line

                yield frame

    def draw_rest_mask(self, mask):
        diagonal = mask.shape[0] + mask.shape[1]
        end_line = self.start_line.offset_line(self.move_dist)
        far_line = self.start_line.offset_line(diagonal)

        mask.fill(0)
        self.fill_lines(mask, end_line, far_line)

    def fill_lines(self, mask, line_start, line_end):
        diagonal = mask.shape[0] + mask.shape[1]

        points = []
        points += list(line_start.get_points(diagonal))
        points += list(line_end.get_points(diagonal))

        for i, point in enumerate(points):
            points[i] = np.int32(point.to_tuple())

        cv2.fillConvexPoly(mask, np.array([points[0], points[2], points[3], points[1]]), 255)


@attr.s
class LineSweeperControls(object):
    sweeper = attr.ib()

    name = attr.ib(default='Line')
    dragging_start = attr.ib(default=False)
    dragging_end = attr.ib(default=False)

    def draw_line(self, draw_list, line, color, diagonal):
        line_start, line_end = line.get_points(diagonal)

        draw_list.add_line(line_start, line_end, color)

    def draw_and_interact(self, draw_list, frame_time, frame_size, input_state):
        diagonal = frame_size.manhattan()

            # do interaction and handles
        start_line = self.sweeper.start_line
        end_line = self.sweeper.line_at_t(1)
            
        mouse_image_pos = draw_list.invert_point(V2.from_tuple(imgui.get_mouse_pos()))
        handle_radius = draw_list.invert_size(5)

        start_handle_pos = start_line.pos
        end_handle_pos = end_line.pos

        if not input_state.mouse_held(0):
            self.dragging_start = False
            self.dragging_end = False
        
        if (not self.dragging_start) and (not self.dragging_end) and input_state.mouse_down(0):
            drag_thresh = handle_radius ** 2

            if is_close(mouse_image_pos, start_handle_pos, handle_radius):
                self.dragging_start = True
            elif is_close(mouse_image_pos, end_handle_pos, handle_radius):
                self.dragging_end = True

        if self.dragging_start:
            start_handle_pos = mouse_image_pos
        elif self.dragging_end:
            end_handle_pos = mouse_image_pos

        self.sweeper.start_line.pos = start_handle_pos
        self.sweeper.move_dist = (end_handle_pos - start_handle_pos).length()
        self.sweeper.start_line.normal_angle = (end_handle_pos - start_handle_pos).angle()

            # actually draw lines, and then handles (order matters for draw order)
        start_line = self.sweeper.line_at_t(0)
        end_line = self.sweeper.line_at_t(1)
        cur_line = self.sweeper.line_at_frame(frame_time)

        cur_line_color = (1,1,1,1)
        handle_line_color = (0.6,0.6,1.0,1)

        self.draw_line(draw_list, start_line, handle_line_color, diagonal)
        self.draw_line(draw_list, end_line, handle_line_color, diagonal)
        self.draw_line(draw_list, cur_line, cur_line_color, diagonal)

        handle_color = (1,1,1,1)
        draw_list.add_circle_filled(start_handle_pos, handle_radius, handle_color)
        draw_list.add_circle_filled(end_handle_pos, handle_radius, handle_color)