import numbers
from math import sqrt, pi, sin, cos, atan2

class V2:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    @classmethod
    def from_angle(cls, angle):
        return cls(cos(angle), sin(angle))

    @classmethod
    def from_tuple(cls, t):
        return cls(t[0], t[1])

    def to_tuple(self):
        return (self.x, self.y)

    def __str__(self):
        return f'({self.x}, {self.y})'

    def __repr__(self):
        return f'V2({self.x!r}, {self.y!r})'

    def __add__(self, other):
        return V2(self.x + other.x, self.y + other.y)

    def __sub__(self, other):
        return V2(self.x - other.x, self.y - other.y)

    def __mul__(self, scalar):
        return V2(self.x * scalar, self.y * scalar)

    def __rmul__(self, scalar):
        return self.__mul__(scalar)

    def __truediv__(self, divisor):
        return V2(self.x / divisor, self.y / divisor)

    def __len__(self):
        return 2

    def __getitem__(self, key):
        if key == 0:
            return self.x
        elif key == 1:
            return self.y
        raise IndexError

    def dot(self, other):
        return self.x * other.x + self.y * other.y

    def length(self):
        return sqrt(self.x * self.x + self.y * self.y)

    def length_sqr(self):
        return self.x * self.x + self.y * self.y

    def manhattan(self):
        return self.x + self.y

    def normalize(self):
        length = self.length()
        if length != 0:
            self.x /= length
            self.y /= length

        return self

    def normalized(self):
        return V2(self.x, self.y).normalize()

    def rotate(self, angle):
        s = sin(angle)
        c = cos(angle)

        x = c * self.x - s * self.y
        y = s * self.x + c * self.y

        self.x = x
        self.y = y

        return self

    def rotated(self, angle):
        return V2(x, y).rotate(angle)

    def rotate90(self):
        x = self.x
        self.x = -self.y
        self.y = x
        
        return self

    def perpendicular(self):
        n = V2(-self.y, self.x)
        return n

    def angle(self):
        return atan2(self.y, self.x)

    def truncate(self):
        self.x = int(self.x)
        self.y = int(self.y)

        return self

