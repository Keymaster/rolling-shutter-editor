import attr
import cv2
from vec2 import V2
@attr.s
class VideoInfo(object):
    frame_size = attr.ib()
    frame_count = attr.ib()
    fps = attr.ib()
    fourcc = attr.ib()

    @classmethod
    def from_capture(cls, capture):
        frame_count = capture.get(cv2.CAP_PROP_FRAME_COUNT)
        fps = int(capture.get(cv2.CAP_PROP_FPS))
        width = int(capture.get(cv2.CAP_PROP_FRAME_WIDTH))
        height = int(capture.get(cv2.CAP_PROP_FRAME_HEIGHT))
        fourcc = int(capture.get(cv2.CAP_PROP_FOURCC))

        return cls(V2(width, height), frame_count, fps, fourcc)

    @property
    def frame_delta(self):
        return 1 / self.fps

    def np_frame_size(self):
        return (self.frame_size.y, self.frame_size.x)