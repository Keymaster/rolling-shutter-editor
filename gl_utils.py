import OpenGL.GL as gl

def alloc_tex():
    tex_id = gl.glGenTextures(1)

        # set filtering
    gl.glBindTexture(gl.GL_TEXTURE_2D, tex_id)
    gl.glTexParameteri(gl.GL_TEXTURE_2D, gl.GL_TEXTURE_MAG_FILTER, gl.GL_LINEAR)
    gl.glTexParameteri(gl.GL_TEXTURE_2D, gl.GL_TEXTURE_MIN_FILTER, gl.GL_LINEAR)
    gl.glBindTexture(gl.GL_TEXTURE_2D, 0)

    return tex_id

def dealloc_tex(tex_id):
    gl.glDeleteTextures([tex_id])

def upload_rgb(tex_id, rgb_data):
    upload_bytes(tex_id, rgb_data, gl.GL_RGB, gl.GL_RGB)

def upload_rgba(tex_id, rgba_data):
    upload_bytes(tex_id, rgba_data, gl.GL_RGBA, gl.GL_RGBA)

def upload_bgra(tex_id, rgba_data):
    upload_bytes(tex_id, rgba_data, gl.GL_BGRA, gl.GL_RGBA)

def upload_bgr(tex_id, bgr_data):
    upload_bytes(tex_id, bgr_data, gl.GL_BGR, gl.GL_RGB)

def upload_bytes(tex_id, bytes_data, input_format, storage_format):
    width = bytes_data.shape[1]
    height = bytes_data.shape[0]

    gl.glBindTexture(gl.GL_TEXTURE_2D, tex_id)
    gl.glPixelStorei(gl.GL_UNPACK_ALIGNMENT, 1)
    gl.glTexImage2D(gl.GL_TEXTURE_2D, 0, storage_format, width, height, 0, input_format, gl.GL_UNSIGNED_BYTE, bytes_data)
    gl.glBindTexture(gl.GL_TEXTURE_2D, 0)