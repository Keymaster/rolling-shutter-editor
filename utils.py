def clamp(v, mi, ma):
    if v < mi:
        return mi
    if v > ma:
        return ma

    return v