import attr
import imgui

@attr.s
class TransformDrawList(object):
    draw_list = attr.ib()
    offset = attr.ib()
    scale = attr.ib()

    def transform_point(self, point):
        return self.offset + point * self.scale

    def invert_point(self, point):
        return (point - self.offset) / self.scale

    def transform_size(self, size):
        return size * self.scale

    def invert_size(self, size):
        return size / self.scale

    def add_line(self, start, end, col):
        start = self.transform_point(start)
        end = self.transform_point(end)

        imgui_col = imgui.get_color_u32_rgba(col[0], col[1], col[2], col[3])
        self.draw_list.add_line(start[0], start[1], end[0], end[1], imgui_col)

    def add_circle_filled(self, center, radius, col, num_segments = 12):
        center = self.transform_point(center)
        radius = self.transform_size(radius)

        imgui_col = imgui.get_color_u32_rgba(col[0], col[1], col[2], col[3])
        self.draw_list.add_circle_filled(center[0], center[1], radius, imgui_col, num_segments)