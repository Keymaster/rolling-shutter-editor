import attr
import cv2
import numpy as np
from math import ceil
# import itertools

@attr.s
class VideoSweeper(object):
    in_reader = attr.ib()
    sweeper = attr.ib()

    frame_data = attr.ib(default=None, init=False)
    _frame_mask = attr.ib(default=None, init=False)
    _mask_iter = attr.ib(default=None, init=False)

    def begin(self):
        self.in_reader.seek_to(self.sweeper.start_frame)

        self.frame_data = self.in_reader.frame_data.copy()
        self._frame_mask = np.zeros(self.frame_data.shape[0:2], dtype=np.uint8)

        self._mask_iter = self.sweeper.mask_iter(self._frame_mask)

        # returns bool indicating whether it has finished or not
    def step_lines(self, num_lines):

        # diagonal = self.in_reader.info.frame_size.manhattan()

        done = False

        for i in range(num_lines):
            frame = None
            try:
                frame = next(self._mask_iter) # also fills _frame_mask with the mask for that frame
            except StopIteration:
                done = True
                break

            self.in_reader.seek_to(frame)

            copy_pixels = self._frame_mask == 255
            self.frame_data[copy_pixels] = self.in_reader.frame_data[copy_pixels]

            # if we're done, fill up the rest of the image
        if done:
            self.sweeper.draw_rest_mask(self._frame_mask)

            copy_pixels = self._frame_mask == 255
            self.frame_data[copy_pixels] = self.in_reader.frame_data[copy_pixels]
        
        return done