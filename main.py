import OpenGL.GL as gl
import numpy as np
import attr
import glfw
import imgui
import cv2
import backend
import imgui_utils
import gl_utils
from collections import namedtuple
from vec2 import V2
from utils import clamp
from line_sweeper import Line, LineSweeper, LineSweeperControls
from ellipse_sweeper import Ellipse, EllipseSweeper, EllipseSweeperControls
from video_player import VideoPlayer
from transform_draw_list import TransformDrawList
from video_sweeper import VideoSweeper
import sys

@attr.s
class Rect(object):
    x = attr.ib()
    y = attr.ib()
    w = attr.ib()
    h = attr.ib()

@attr.s
class VideoDisplayLayout(object):
    window_rect = attr.ib()
    video_size = attr.ib()
    video_rect = attr.ib()
    video_screen_scale = attr.ib()

@attr.s
class ControlsLayout(object):
    window_rect = attr.ib()
    icon_size = attr.ib()
    icons_left = attr.ib()
    icons_spacing = attr.ib()

class Editor():
    def __init__(self, window_size, path):
        self.window_size = window_size
        self.path = path

    def setup(self):
        self.video_player = VideoPlayer(self.path)
        self.calculate_layout_sizes()

        self.video_tex_id = gl_utils.alloc_tex()
        self.update_video_frame()

        self.buttons_tex_id = gl_utils.alloc_tex()
        buttons_bgra = cv2.imread('icons/icons.png', cv2.IMREAD_UNCHANGED)
        gl_utils.upload_bgra(self.buttons_tex_id, buttons_bgra)

        self.sweeper_controls = []

        line = Line(V2(100, 100), np.pi / 4)
        line_sweeper = LineSweeper(
            line,
            500,
            60,
            240)

        line_controls = LineSweeperControls(line_sweeper)
        self.sweeper_controls.append(line_controls)

        ellipse = Ellipse(V2(960, 640), V2(30, 70), np.pi / 3)
        ellipse_sweeper = EllipseSweeper(ellipse, 2, 60, 240)

        ellipse_controls = EllipseSweeperControls(ellipse_sweeper)
        self.sweeper_controls.append(ellipse_controls)
        
        self.selected_sweeper = -1

        np_frame_shape = self.video_player.reader.info.np_frame_size()
        self.mask_frame = np.zeros(np_frame_shape, dtype=np.uint8)
        self.sweeped_frame = np.zeros((np_frame_shape[0], np_frame_shape[1], 3), dtype=np.uint8)

        self.show_sweeped = False

        self.prev_mouse_pos = None

    def calculate_layout_sizes(self):
        window_w, window_h = self.window_size

        controls_bar_h = 0.05 * window_h
        controls_icon_size = 0.9 * controls_bar_h
        controls_icons_spacing = 0.1 * controls_bar_h
        controls_buttons_w = controls_icon_size + controls_icons_spacing + controls_icon_size
        controls_icons_left = window_w - controls_buttons_w

        self.controls_layout = ControlsLayout(
            window_rect = Rect(0, window_h - controls_bar_h, window_w, controls_bar_h),
            icon_size = controls_icon_size,
            icons_left = controls_icons_left,
            icons_spacing = controls_icons_spacing)

        sweepers_panel_w = window_w * 0.3
        sweepers_panel_h = window_h - controls_bar_h

        self.sweeper_controls_panel_layout = Rect(window_w - sweepers_panel_w, 0, sweepers_panel_w, sweepers_panel_h)

        frame_w, frame_h = self.video_player.reader.info.frame_size
        video_player_h = window_h - controls_bar_h
        video_player_w = window_w - sweepers_panel_w

        x_fit_scale = video_player_w / frame_w
        y_fit_scale = video_player_h / frame_h

        fit_scale = min(x_fit_scale, y_fit_scale)

        video_display_w = frame_w * fit_scale
        video_display_h = frame_h * fit_scale

        video_display_x = (video_player_w / 2) - (video_display_w / 2)
        video_display_y = (video_player_h / 2) - (video_display_h / 2)

        self.video_display_layout = VideoDisplayLayout(
            window_rect = Rect(0, 0, video_player_w, video_player_h),
            video_size = self.video_player.reader.info.frame_size,
            video_rect = Rect(video_display_x, video_display_y, video_display_w, video_display_h),
            video_screen_scale = fit_scale)

    def update_video_frame(self):
        gl_utils.upload_bgr(self.video_tex_id, self.video_player.reader.frame_data)

    def draw_frame(self, input_state):
        io = imgui.get_io()
        delta_time = clamp(io.delta_time, 0, 1/20)

        if self.show_sweeped:
            sweeping_done = self.video_sweeper.step_lines(10)
            gl_utils.upload_bgr(self.video_tex_id, self.video_sweeper.frame_data)
            if sweeping_done:
                cv2.imwrite('img_out.png', self.video_sweeper.frame_data)
                self.video_sweeper = None
                self.show_sweeped = False

        new_frame = False
        if not self.show_sweeped:
            new_frame = self.video_player.step_time(io.delta_time)

            if input_state.key_down(glfw.KEY_LEFT):
                if input_state.super:
                    self.video_player.seek_to_start()
                else:
                    self.video_player.prev_frame()
                new_frame = True

            if input_state.key_down(glfw.KEY_RIGHT):
                if input_state.super:
                    self.video_player.seek_to_end()
                else:
                    self.video_player.next_frame()
                new_frame = True

            if input_state.key_down(glfw.KEY_SPACE):
                self.video_player.paused = not self.video_player.paused

        with imgui.istyled(*imgui_utils.flat_window_style):
            self.draw_sweepers_panel()
            self.draw_video_display(input_state)
            controls_changed_frame = self.draw_controls()
            new_frame = new_frame or controls_changed_frame

        if not self.show_sweeped:
            if new_frame:
                self.update_video_frame()

        # if input_state.key_down(glfw.KEY_S) and input_state.super:
        #     cv2.imwrite('img_out.png', self.sweeped_frame)
        #     print(self.sweeper_controls[0].sweeper.start_line.normal_angle)

        # imgui.show_demo_window()

        if input_state.key_down(glfw.KEY_ESCAPE):
            return False
        return True

    def draw_sweepers_panel(self):
        imgui.set_next_window_position(self.sweeper_controls_panel_layout.x, self.sweeper_controls_panel_layout.y)
        imgui.set_next_window_size(self.sweeper_controls_panel_layout.w, self.sweeper_controls_panel_layout.h)
        imgui.begin('sweepers', flags = imgui.WINDOW_NO_RESIZE | imgui.WINDOW_NO_MOVE | imgui.WINDOW_NO_COLLAPSE)

        for i, sweeper_state in enumerate(self.sweeper_controls):
            sweeper = sweeper_state.sweeper

            label = sweeper_state.name
            selected = self.selected_sweeper == i

            clicked, selected = imgui.selectable(label, selected)
            if clicked:
                self.selected_sweeper = i

            if selected:
                imgui.indent()
                
                imgui.text('Start frame:')
                imgui.same_line()
                imgui.push_item_width(-100)
                start_changed, start_frame = imgui.input_int('##start_frame_input', sweeper.start_frame)
                imgui.pop_item_width()
                imgui.same_line()
                set_start = imgui.button('Set current##set_start_frame_button')

                if set_start:
                    start_changed = True
                    start_frame = self.video_player.reader.frame_idx

                if start_changed:
                    end_frame = sweeper.start_frame + sweeper.move_time

                    start_frame = clamp(start_frame, 0, self.video_player.reader.info.frame_count - 2)
                    sweeper.start_frame = start_frame # problem: namedtuple is not mutable

                    if end_frame > sweeper.start_frame:
                        sweeper.move_time = end_frame - sweeper.start_frame
                    else:
                        sweeper.move_time = 1

                imgui.text('End frame:  ')
                imgui.same_line()
                imgui.push_item_width(-100)
                end_changed, end_frame = imgui.input_int('##end_frame_input', sweeper.start_frame + sweeper.move_time)
                imgui.pop_item_width()
                imgui.same_line()
                set_end = imgui.button('Set current##set_end_frame_button')

                if set_end:
                    end_changed = True
                    end_frame = self.video_player.reader.frame_idx

                if end_changed:
                    end_frame = clamp(end_frame, sweeper.start_frame + 1, self.video_player.reader.info.frame_count - 1)
                    sweeper.move_time = end_frame - sweeper.start_frame

                imgui.unindent()

        imgui.end()

    def draw_video_display(self, input_state):
        video_window_rect = self.video_display_layout.window_rect
        imgui.set_next_window_position(video_window_rect.x, video_window_rect.y)
        imgui.set_next_window_size(video_window_rect.w, video_window_rect.h)

        video_display_rect = self.video_display_layout.video_rect
        imgui.begin('video player', flags = imgui_utils.box_window_flags | imgui.WINDOW_NO_SCROLL_WITH_MOUSE)

        if not input_state.mouse_held(2):
            self.dragging_video = False
            self.prev_mouse_pos = None

        if input_state.mouse_down(2) and imgui.is_window_hovered():
            self.dragging_video = True

        if self.dragging_video:
            mouse_pos = V2.from_tuple(imgui.get_mouse_pos())
            if self.prev_mouse_pos is None:
                self.prev_mouse_pos = mouse_pos

            move_delta = mouse_pos - self.prev_mouse_pos

            video_display_rect.x += move_delta.x
            video_display_rect.y += move_delta.y

            self.prev_mouse_pos = mouse_pos

        io = imgui.get_io()
        if io.mouse_wheel != 0:
            prev_scale = self.video_display_layout.video_screen_scale

            new_scale  = prev_scale
            new_scale += io.mouse_wheel * 0.01
            new_scale  = clamp(new_scale, 0.01, 10)
            self.video_display_layout.video_screen_scale = new_scale

            mouse_pos = V2.from_tuple(imgui.get_mouse_pos())
            mouse_rel_corner = V2(video_display_rect.x, video_display_rect.y) - mouse_pos
            mouse_rel_corner *= new_scale / prev_scale
            mouse_rel_corner += mouse_pos

            video_display_rect.x = mouse_rel_corner.x
            video_display_rect.y = mouse_rel_corner.y

            new_size = self.video_display_layout.video_size * new_scale

            video_display_rect.w = new_size.x
            video_display_rect.h = new_size.y

        imgui.set_cursor_pos((video_display_rect.x, video_display_rect.y))

        # imgui.begin_child('video', video_display_rect.w, video_display_rect.h)
        
        imgui.image(self.video_tex_id, video_display_rect.w, video_display_rect.h)

        self.draw_sweeper_lines(input_state)

        # imgui.end_child()

        imgui.end()

    def draw_sweeper_lines(self, input_state):
        imgui_draw_list = imgui.get_window_draw_list()
        video_offset = V2(self.video_display_layout.video_rect.x, self.video_display_layout.video_rect.y)
        video_scale = self.video_display_layout.video_screen_scale

        draw_list = TransformDrawList(imgui_draw_list, video_offset, video_scale)

        frame_time = self.video_player.get_decimal_frame()

        if self.selected_sweeper != -1:
            sweeper_control = self.sweeper_controls[self.selected_sweeper]
            sweeper_control.draw_and_interact(draw_list, frame_time, self.video_player.reader.info.frame_size, input_state)

    def draw_controls(self):
        controls_window_rect = self.controls_layout.window_rect
        imgui.set_next_window_position(controls_window_rect.x, controls_window_rect.y)
        imgui.set_next_window_size(controls_window_rect.w, controls_window_rect.h)
        
        imgui.begin('player controls', flags = imgui_utils.box_window_flags)

        imgui.same_line(self.controls_layout.icons_left - 100)
        if imgui.button('Do sweep'):
            if self.selected_sweeper != -1 and (not self.show_sweeped):
                self.show_sweeped = True
                self.video_sweeper = VideoSweeper(self.video_player.reader, self.sweeper_controls[self.selected_sweeper].sweeper)
                self.video_sweeper.begin()

        imgui.set_cursor_pos((self.controls_layout.icons_left, 0))

        imgui.push_id("pause_btn")
        pause_clicked = imgui.image_button(self.buttons_tex_id, self.controls_layout.icon_size, self.controls_layout.icon_size, uv0=(0.5,0.0), uv1=(1.0, 1.0), tint_color=(0,0,0,1), frame_padding=0)
        imgui.pop_id()

        if pause_clicked:
            self.video_player.paused = True
        
        imgui.same_line(spacing = self.controls_layout.icons_spacing)
        imgui.push_id("play_btn")
        play_clicked = imgui.image_button(self.buttons_tex_id, self.controls_layout.icon_size, self.controls_layout.icon_size, uv0=(0.0,0.0), uv1=(0.5, 1.0), tint_color=(0,0,0,1), frame_padding=0)
        imgui.pop_id()

        imgui.set_cursor_pos((0, 0))

        imgui.push_item_width(self.controls_layout.icons_left - 120) # estimating width of label manually
        frame_idx_changed, new_frame_idx = imgui.slider_int('##frame_idx', self.video_player.reader.frame_idx, 0, self.video_player.reader.info.frame_count, '%d')
        imgui.pop_item_width()

        if frame_idx_changed:
            self.video_player.paused = True
            self.video_player.seek_to(new_frame_idx)

        if play_clicked:
            self.video_player.paused = False

        imgui.end()

        return frame_idx_changed

    def cleanup(self):
        self.video_player.cleanup()
        gl_utils.dealloc_tex(self.video_tex_id)

def main(path):
    window_size = (1280, 720)

    editor = Editor(window_size, path)
    backend.run(editor.setup, editor.draw_frame, editor.cleanup, window_size=window_size)

if __name__ == '__main__':
    path = None
    if len(sys.argv) < 2:
        path = 'fan_trimmed.mp4'
    else:
        path = sys.argv[1]

    main(path)
