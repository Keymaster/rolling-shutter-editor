import imgui

flat_window_style = (imgui.STYLE_WINDOW_ROUNDING, 0, imgui.STYLE_WINDOW_PADDING, (0,0), imgui.STYLE_WINDOW_BORDERSIZE, 0)
box_window_flags = imgui.WINDOW_NO_TITLE_BAR | imgui.WINDOW_NO_RESIZE | imgui.WINDOW_NO_MOVE | imgui.WINDOW_NO_SCROLLBAR | imgui.WINDOW_NO_COLLAPSE